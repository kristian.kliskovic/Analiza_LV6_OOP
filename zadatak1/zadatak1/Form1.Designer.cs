﻿namespace zadatak1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.naziv = new System.Windows.Forms.Label();
            this.button_log10 = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_minus = new System.Windows.Forms.Button();
            this.button_ln = new System.Windows.Forms.Button();
            this.button_mnozenje = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_dijeljenje = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button_clear = new System.Windows.Forms.Button();
            this.button_quit = new System.Windows.Forms.Button();
            this.rjesenje = new System.Windows.Forms.Label();
            this.button_plus = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // naziv
            // 
            this.naziv.AutoSize = true;
            this.naziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.naziv.Location = new System.Drawing.Point(12, 12);
            this.naziv.Name = "naziv";
            this.naziv.Size = new System.Drawing.Size(126, 29);
            this.naziv.TabIndex = 0;
            this.naziv.Text = "Kalkulator";
            this.naziv.Click += new System.EventHandler(this.label1_Click);
            // 
            // button_log10
            // 
            this.button_log10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_log10.Location = new System.Drawing.Point(298, 12);
            this.button_log10.Name = "button_log10";
            this.button_log10.Size = new System.Drawing.Size(93, 58);
            this.button_log10.TabIndex = 2;
            this.button_log10.Text = "log10";
            this.button_log10.UseVisualStyleBackColor = true;
            this.button_log10.Click += new System.EventHandler(this.button_log10_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_sqrt.Location = new System.Drawing.Point(298, 65);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(93, 53);
            this.button_sqrt.TabIndex = 3;
            this.button_sqrt.Text = "sqrt";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button3_Click);
            // 
            // button_minus
            // 
            this.button_minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_minus.Location = new System.Drawing.Point(210, 111);
            this.button_minus.Margin = new System.Windows.Forms.Padding(0);
            this.button_minus.Name = "button_minus";
            this.button_minus.Size = new System.Drawing.Size(93, 51);
            this.button_minus.TabIndex = 0;
            this.button_minus.Text = "-";
            this.button_minus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button_minus.UseVisualStyleBackColor = true;
            this.button_minus.Click += new System.EventHandler(this.button4_Click);
            // 
            // button_ln
            // 
            this.button_ln.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_ln.Location = new System.Drawing.Point(298, 111);
            this.button_ln.Name = "button_ln";
            this.button_ln.Size = new System.Drawing.Size(93, 51);
            this.button_ln.TabIndex = 5;
            this.button_ln.Text = "ln";
            this.button_ln.UseVisualStyleBackColor = true;
            this.button_ln.Click += new System.EventHandler(this.button_ln_Click);
            // 
            // button_mnozenje
            // 
            this.button_mnozenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_mnozenje.Location = new System.Drawing.Point(210, 157);
            this.button_mnozenje.Name = "button_mnozenje";
            this.button_mnozenje.Size = new System.Drawing.Size(93, 51);
            this.button_mnozenje.TabIndex = 6;
            this.button_mnozenje.Text = "x";
            this.button_mnozenje.UseVisualStyleBackColor = true;
            this.button_mnozenje.Click += new System.EventHandler(this.button_mnozenje_Click);
            // 
            // button_sin
            // 
            this.button_sin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_sin.Location = new System.Drawing.Point(298, 157);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(93, 51);
            this.button_sin.TabIndex = 7;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_dijeljenje
            // 
            this.button_dijeljenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_dijeljenje.Location = new System.Drawing.Point(210, 203);
            this.button_dijeljenje.Name = "button_dijeljenje";
            this.button_dijeljenje.Size = new System.Drawing.Size(93, 51);
            this.button_dijeljenje.TabIndex = 8;
            this.button_dijeljenje.Text = "/";
            this.button_dijeljenje.UseVisualStyleBackColor = true;
            this.button_dijeljenje.Click += new System.EventHandler(this.button_dijeljenje_Click);
            // 
            // button_cos
            // 
            this.button_cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_cos.Location = new System.Drawing.Point(298, 203);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(93, 51);
            this.button_cos.TabIndex = 9;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 65);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(136, 22);
            this.textBox1.TabIndex = 10;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 111);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(135, 22);
            this.textBox2.TabIndex = 11;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // button_clear
            // 
            this.button_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_clear.Location = new System.Drawing.Point(12, 236);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(82, 33);
            this.button_clear.TabIndex = 14;
            this.button_clear.Text = "Clear";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button_quit
            // 
            this.button_quit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_quit.Location = new System.Drawing.Point(226, 264);
            this.button_quit.Name = "button_quit";
            this.button_quit.Size = new System.Drawing.Size(140, 37);
            this.button_quit.TabIndex = 15;
            this.button_quit.Text = "Zatvaranje";
            this.button_quit.UseVisualStyleBackColor = true;
            this.button_quit.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // rjesenje
            // 
            this.rjesenje.AutoSize = true;
            this.rjesenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rjesenje.Location = new System.Drawing.Point(12, 179);
            this.rjesenje.Name = "rjesenje";
            this.rjesenje.Size = new System.Drawing.Size(0, 29);
            this.rjesenje.TabIndex = 16;
            this.rjesenje.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // button_plus
            // 
            this.button_plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_plus.Location = new System.Drawing.Point(210, 12);
            this.button_plus.Name = "button_plus";
            this.button_plus.Size = new System.Drawing.Size(93, 112);
            this.button_plus.TabIndex = 17;
            this.button_plus.Text = "+";
            this.button_plus.UseVisualStyleBackColor = true;
            this.button_plus.Click += new System.EventHandler(this.button1_Click_3);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(114, 236);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 34);
            this.button1.TabIndex = 18;
            this.button1.Text = "Ans";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Rezultat: ";
            this.label1.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 313);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_dijeljenje);
            this.Controls.Add(this.button_mnozenje);
            this.Controls.Add(this.button_minus);
            this.Controls.Add(this.button_plus);
            this.Controls.Add(this.rjesenje);
            this.Controls.Add(this.button_quit);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_ln);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_log10);
            this.Controls.Add(this.naziv);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label naziv;
        private System.Windows.Forms.Button button_log10;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_minus;
        private System.Windows.Forms.Button button_ln;
        private System.Windows.Forms.Button button_mnozenje;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_dijeljenje;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Button button_quit;
        private System.Windows.Forms.Label rjesenje;
        private System.Windows.Forms.Button button_plus;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
    }
}

